#!/bin/bash
set -e

name=testng
version="$(sed -n 's/Version:\s*//p' *.spec)"
upstream_version="$(echo ${version} | tr '~' '-')"

# RETRIEVE
wget "https://github.com/cbeust/testng/archive/${upstream_version}.tar.gz" -O "${name}-${upstream_version}.orig.tar.gz"

rm -rf tarball-tmp
mkdir tarball-tmp
cd tarball-tmp
tar xf "../${name}-${upstream_version}.orig.tar.gz"

# CLEAN TARBALL
rm -r */gradle* */kobalt*
rm */src/main/resources/org/testng/jquery-*.js

tar cf "../${name}-${version}.tar.gz" *
cd ..
rm -r tarball-tmp "${name}-${upstream_version}.orig.tar.gz"
